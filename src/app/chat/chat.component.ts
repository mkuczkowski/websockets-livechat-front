import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  numberOfUsers: number;

  message: string;
  messages: string[] = [];
  currentUser: string;

  constructor(private callService: CallService) {
    const randomName = Math.random().toString(36).substring(7);
    this.currentUser = randomName;
  }

  sendMessage() {
    this.callService.sendMessage(this.currentUser + ': ' + this.message);
    this.message = '';
  }

  isEmpty(): Boolean {
    return this.message === '' || this.message === null || this.message === undefined;
  }

  isConnectedInfo(msg): Boolean {
    return msg === 'User connected!';
  }

  isDisconnectedInfo(msg): Boolean {
    return msg === 'User disconnected!';
  }

  isNicknameEmpty(): Boolean {
    return this.currentUser === '' || this.currentUser === null || this.currentUser === undefined;
  }

  ngOnInit(){
    this.callService
      .getMessages()
      .subscribe((message: string) => {
        this.messages.push(message);
      });
    this.callService
      .getNotifications()
      .subscribe((info: number) => {
        this.numberOfUsers = info;
      });
  }

}
