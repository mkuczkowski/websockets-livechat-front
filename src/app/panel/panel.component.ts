import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  currentWindow: string = "chat";

  constructor() { }

  ngOnInit() {
  }

  isChatEnabled(): Boolean {
    return this.currentWindow == "chat";
  }

  switchToChat() {
    this.currentWindow = "chat";
  }

  switchToDialer() {
    this.currentWindow = "dialer";
  }

}
