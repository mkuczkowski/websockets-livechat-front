import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PanelComponent } from './panel/panel.component';
import { WidgetComponent } from './widget/widget.component';
import { FormsModule } from '@angular/forms';
import { CallService } from './call.service';
import { HttpClientModule } from '@angular/common/http';
import { ChatComponent } from './chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    PanelComponent,
    WidgetComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [CallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
